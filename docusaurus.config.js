// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Clara Mantelli',
  tagline: 'CEEJA',
  favicon: 'img/shelf-32x32.jpg',

  url: 'https://mantelli.saopaulo.download',
  baseUrl: '/',

  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'mantellidownload', // Usually your repo name.

  plugins: [require.resolve('docusaurus-lunr-search')],
  noIndex: true,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  // https://github.com/facebook/docusaurus/tree/main/packages/docusaurus-theme-translations/locales
  i18n: {
    defaultLocale: 'pt-BR',
    locales: ['en', 'pt-BR'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          //editUrl:
          //  'https://gitlab.com',
          routeBasePath: '/',
          showLastUpdateTime: true,
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/shelf-256x256.jpg',
      navbar: {
        title: 'Clara Mantelli',
        logo: {
          alt: 'My Site Logo',
          src: 'img/shelf-32x32.jpg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Docs',
          },
          //{to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        //links: [
        //  {
        //    title: 'Docs',
        //    items: [
        //      {
        //        label: 'Tutorial',
        //        to: '/docs/intro',
        //      },
        //    ],
        //  },
        //  {
        //    title: 'Community',
        //    items: [
        //      {
        //        label: 'Stack Overflow',
        //        href: 'https://stackoverflow.com/questions/tagged/docusaurus',
        //      },
        //      {
        //        label: 'Discord',
        //        href: 'https://discordapp.com/invite/docusaurus',
        //      },
        //      {
        //        label: 'Twitter',
        //        href: 'https://twitter.com/docusaurus',
        //      },
        //    ],
        //  },
        //  {
        //    title: 'More',
        //    items: [
        //      {
        //        label: 'Blog',
        //        to: '/blog',
        //      },
        //      {
        //        label: 'GitHub',
        //        href: 'https://github.com/facebook/docusaurus',
        //      },
        //    ],
        //  },
        //],
        copyright: `Copyright © ${new Date().getFullYear()} Alunos, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: false,
        respectPrefersColorScheme: false,
    },
    }),
};

export default config;
