---
slug: /
sidebar_position: 1
---

# Intro
Link do GoogleMaps:  
[maps.app.goo.gl/UvJPLYMEXXmDCUYeA](https://maps.app.goo.gl/UvJPLYMEXXmDCUYeA)

Descer na estacao Belem da linha vermelha do Metro, descer a passarela e seguir
em direcao a Tatuape. São 5 minutos caminhando desde a estacao Belem até o
CEEJA.  
O sistema é totalmente presencial. Essa escola não oferece Educacao a distancia
(EaD).  
Endereco do Ceeja: Av. Alcântara Machado, 4188 - Belem, São Paulo - SP, 03101-005  

---

*Esse site não é um site oficial da escola.*
